using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ListController : MonoBehaviour
{
    public List<ElementData> currentDataList = new List<ElementData>();
    [SerializeField] ListSlotHandler listSlotPrefab;
    List<ListSlotHandler> listSlots = new List<ListSlotHandler>();
    [SerializeField] ListElementHandler listElementPrefab;
    [SerializeField] Transform contentParent;
    [SerializeField] TMP_Text elementCounterText;
    //Sorting
    [SerializeField] Button sortTypeButton;
    [SerializeField] Button sortDirectionButton;
    [SerializeField] TMP_Text sortTypeButtonText;
    [SerializeField] TMP_Text sortDirectionButtonText;
    bool isAscendingSorting = true;
    bool isByStringSorting = true;

    public void InstantiateSlots(int count)
    {
        for (int i = 0; i < count; i++)
        {
            ListSlotHandler listSlotHandler = Instantiate(listSlotPrefab, contentParent);
            listSlotHandler.listController = this;
            listSlots.Add(listSlotHandler);
        }
    }
    public void UpdateUI(List<ElementData> newElementDataList)
    {
        foreach (var t in listSlots)
        {
            if (t.transform.childCount > 0)
            {
                GameObject.Destroy(t.transform.GetChild(0).gameObject);
            }
        }
        for (int i = 0; i < newElementDataList.Count; i++)
        {
            ListElementHandler listElement = Instantiate(listElementPrefab, listSlots[i].transform);
            listElement.SetValue(this, newElementDataList[i].stringValue, newElementDataList[i].numberValue);
        }
        currentDataList = newElementDataList;
        elementCounterText.text = gameObject.name + ", elements: " + currentDataList.Count;
    }
    void Start()
    {
        isAscendingSorting = true;
        isByStringSorting = true;
        sortTypeButtonText.text = isByStringSorting ? "ByString" : "ByNumber";
        sortDirectionButtonText.text = isAscendingSorting ? "Ascending" : "Descending";
        sortTypeButton.onClick.AddListener(OnSortTypeButtonPressed);
        sortDirectionButton.onClick.AddListener(OnSortDirectionButtonPressed);
    }
    void OnSortTypeButtonPressed()
    {
        isByStringSorting = !isByStringSorting;
        sortTypeButtonText.text = isByStringSorting ? "ByString" : "ByNumber";
        SortElements();
    }
    void OnSortDirectionButtonPressed()
    {
        isAscendingSorting = !isAscendingSorting;
        sortDirectionButtonText.text = isAscendingSorting ? "Ascending" : "Descending";
        SortElements();
    }


    void SortElements()
    {
        var newList = currentDataList;
        if (isByStringSorting)
        {
            if (isAscendingSorting)
            {
                newList.Sort((a, b) => a.stringValue.CompareTo(b.stringValue));
            }
            else
            {
                newList.Sort((a, b) => b.stringValue.CompareTo(a.stringValue));
            }
        }
        else
        {
            if (isAscendingSorting)
            {
                newList.Sort((a, b) => a.numberValue.CompareTo(b.numberValue));
            }
            else
            {
                newList.Sort((a, b) => b.numberValue.CompareTo(a.numberValue));
            }
        }
        UpdateUI(newList);
    }
}