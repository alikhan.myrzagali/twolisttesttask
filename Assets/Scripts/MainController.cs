using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
[System.Serializable]
public class ListsContainer
{
    public List<ElementData> firstList;
    public List<ElementData> secondList;
}

[System.Serializable]
public class ElementData
{
    public string stringValue;
    public int numberValue;
}

public class MainController : MonoBehaviour
{
    public static MainController Instance;
    int elementsCount = 10;
    string jsonPath;
    [SerializeField] ListController firstListController;
    [SerializeField] ListController secondListController;
    [SerializeField] Button saveToJsonButton;
    void Awake()
    {
        jsonPath = Application.dataPath + "/Data/jsonData.json";
        Instance = this;
        firstListController.InstantiateSlots(elementsCount);
        secondListController.InstantiateSlots(elementsCount);
    }
    void Start()
    {
        LoadFromJson();
        saveToJsonButton.onClick.AddListener(SaveToJson);
    }

    void SaveToJson()
    {
        ListsContainer listsContainer = new ListsContainer();
        listsContainer.firstList = firstListController.currentDataList;
        listsContainer.secondList = secondListController.currentDataList;
        string jsonText = JsonUtility.ToJson(listsContainer, true);
        File.WriteAllText(jsonPath, jsonText);
        Debug.Log("Saved to Json");
    }
    void LoadFromJson()
    {
        string jsonText = File.ReadAllText(jsonPath);
        ListsContainer listsContainer = JsonUtility.FromJson<ListsContainer>(jsonText);
        firstListController.currentDataList = listsContainer.firstList;
        secondListController.currentDataList = listsContainer.secondList;
        RefreshLists();
    }
    public void RefreshLists()
    {
        firstListController.UpdateUI(firstListController.currentDataList);
        secondListController.UpdateUI(secondListController.currentDataList);
    }

    public void ChangeOrderWithinList(ListController list, int oldIndex, int newIndex)
    {
        ElementData elementData = list.currentDataList[oldIndex];
        list.currentDataList.RemoveAt(oldIndex);
        list.currentDataList.Insert(newIndex, elementData);
        list.UpdateUI(list.currentDataList);
    }
    public void SetAsLastWithinList(ListController list, int initIndex)
    {
        ElementData elementData = list.currentDataList[initIndex];
        list.currentDataList.RemoveAt(initIndex);
        list.currentDataList.Add(elementData);
        list.UpdateUI(list.currentDataList);
    }
    public void DeleteFromOldAndInsertIntoNew(ListController oldList, ListController newList, int oldIndex, int newIndex)
    {
        ElementData elementData = oldList.currentDataList[oldIndex];
        oldList.currentDataList.RemoveAt(oldIndex);
        newList.currentDataList.Insert(newIndex, elementData);
        RefreshLists();
    }
    public void SetAsLastInNew(ListController oldList, ListController newList, int initIndex)
    {
        ElementData elementData = oldList.currentDataList[initIndex];
        oldList.currentDataList.RemoveAt(initIndex);
        newList.currentDataList.Add(elementData);
        RefreshLists();
    }

}
