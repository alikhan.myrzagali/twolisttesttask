using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ListElementHandler : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    [HideInInspector] Transform parentAfterDrag;
    [SerializeField] TMP_Text textField;
    ListController listController;
    int initIndex;
    public void SetValue(ListController listController, string stringValue, int numberValue)
    {
        this.listController = listController;
        textField.text = $"{stringValue} - {numberValue}";
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        parentAfterDrag = transform.parent;
        initIndex = transform.parent.GetSiblingIndex();
        transform.SetParent(transform.root);
        transform.SetAsLastSibling();
        textField.raycastTarget = false;
    }
    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(parentAfterDrag);
        var dropObject = eventData.pointerCurrentRaycast.gameObject;
        textField.raycastTarget = true;
        if (dropObject==null)
        {
            return;
        }
        if (dropObject.TryGetComponent<ListElementHandler>(out ListElementHandler listElementHandler))
        {
            int newIndex = listElementHandler.transform.parent.GetSiblingIndex();
            if (listElementHandler.listController == this.listController)
            {
                MainController.Instance.ChangeOrderWithinList(listElementHandler.listController, initIndex, newIndex);
            }
            else
            {
                MainController.Instance.DeleteFromOldAndInsertIntoNew(this.listController, listElementHandler.listController, initIndex, newIndex);
            }
        }
        else if (dropObject.TryGetComponent<ListSlotHandler>(out ListSlotHandler listSlotHandler))
        {
            if (this.listController == listSlotHandler.listController)
            {
                MainController.Instance.SetAsLastWithinList(listSlotHandler.listController, initIndex);
            }
            else
            {
                MainController.Instance.SetAsLastInNew(this.listController, listSlotHandler.listController, initIndex);
            }
        }
    }

}
